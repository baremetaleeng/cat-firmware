# CATracker Firmware
Die STM32 Firmware für CATracker, ein "Full Stack" Sensor IOT Projekt

## TODO
* Teil 1
    - [X] CubeIDE Projekt anlegen
    - [X] Konfiguration der HAL über Editor
    - [X] Einrichten von FreeRTOS
    - [X] Blinky -> Hardware
* Teil 2
    - [X] Software Komponenten
        - [X] Kommunikation zwischen LoRa Radio und MCU - SPI (LORA MAC)
        - [X] Parsen NMEA Daten 
        - [X] Beschleunigungssensor
        - [X] Batteriemanagement
    - [X] STM32 HAL Treiber
    - [X ] ADC
        - [X] Continuous / Discontinuous Mode?
        - [X] ACD Konvertierung
    - [X] UART
    - [X] DMA
    - [X] Timer
    - [X] SPI
    - [X] I2C
    - [X] Blinky flashen
    - [X] GPS Modul
        - [X] RX ISR und parsen der NMEA Daten
            - [X] Satz einlesen von $ bis \n
            - [X] Satz über RTT ausgeben
            - [X] Kompletten Satz in den Parser kopieren
        - [X] -> Tools - GnssToolKit3
        - [X] Treiber für Firmware
* Teil3
    - [X] Batterie Management
        - [X] ADC einstellen
        - [X] Treiber für Firmware
    - [X] Bewegungssensor
        - [ ] Tools 
        - [ ] Treiber für Firmware
* Teil 4
    - [X] LoRaWAN Stack
        - [X] LoRaWAN MAC einbinden
        - [X] Timing konfigurieren
        - [X] TTN Integration -> Backend
        - [X] Mit festen Werten
        - [X] JOIN im Netzwerk
        - [X] Cayenne Low Power Payload (LPP)
    - [X] Freigabe Firmware Rev1
* Teil 5
    - [ ] Batterie Management
        - [X] Low Power Mode
        - [X] Entladekurven aufnehmen
        - [ ] Ladezustand errechnen
* Teil 6
    - [ ] GPS Modul
        - [ ] Low Power Modes
        - [ ] LED Funktion
        - [ ] Warm/Cold Start
        - [ ] Baud Raten Umschaltung
* Teil 7
    - [ ] FUOTA - Firmware Update over the air
        - [ ] Anforderungen
        - [ ] tbd
    - [ ] Freigabe Firmware Rev2
* Teil 8
    - [ ] Bewegungssensor
        - [ ] ISR für das antriggern einer neuen Positionierung
        - [ ] Klassifizierung der Aktion
        - [ ] Unknown Klassifizierung
* [ ] Freigabe Firmware Rev3


