/*
 * post.c - POWER ON SELF TEST
 *
 *  Created on: May 25, 2020
 *      Author: bastian
 */

#include "tasks.h"
#include "main.h"

extern ADC_HandleTypeDef hadc;
extern I2C_HandleTypeDef hi2c1;
extern TIM_HandleTypeDef htim2;

uint8_t error;
void POST_run() {
	uint8_t val;

	val = HAL_GPIO_ReadPin(CHRG_GPIO_Port, CHRG_Pin);
	SEGGER_RTT_printf(0, "CHRG [1]: %d\n\r", val); // HIGH
	if (GPIO_PIN_SET != val) {
		error++;
	}

	val = HAL_GPIO_ReadPin(POWER_PG_GPIO_Port, POWER_PG_Pin); // HIGH
	SEGGER_RTT_printf(0, "POWER_PG [1]: %d\n\r", val);
	if (GPIO_PIN_SET != val) {
		error++;
	}

	val = HAL_GPIO_ReadPin(IMU_INT1_GPIO_Port, IMU_INT1_Pin); // LOW
	SEGGER_RTT_printf(0, "IMU_INT1 [0]: %d\n\r", val);
	if (GPIO_PIN_RESET != val) {
		error++;
	}

	val = HAL_GPIO_ReadPin(IMU_INT2_GPIO_Port, IMU_INT2_Pin); // LOW
	SEGGER_RTT_printf(0, "IMU_INT2 [0]: %d\n\r", val);
	if (GPIO_PIN_RESET != val) {
		error++;
	}

	// I2C1_SCL
	val = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
	SEGGER_RTT_printf(0, "I2C_SCL [1]: %d\n\r", val);
	if (GPIO_PIN_SET != val) {
		error++;
	}

	// I2C1_SDA
	val = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
	SEGGER_RTT_printf(0, "I2C_SDA [1]: %d\n\r", val);
	if (GPIO_PIN_SET != val) {
		error++;
	}

	// EEPROM_WP
	val = HAL_GPIO_ReadPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
	SEGGER_RTT_printf(0, "EEPROM_WP [0]: %d\n\r", val);
	if (GPIO_PIN_RESET != val) {
		error++;
	}

	// GPS_RX
	val = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9);
	SEGGER_RTT_printf(0, "GPS_RX [1]: %d\n\r", val);
	if (GPIO_PIN_SET != val) {
		error++;
	}

	// GPS_TX
	val = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10);
	SEGGER_RTT_printf(0, "GPS_TX [0]: %d\n\r", val);
	if (GPIO_PIN_RESET != val) {
		error++;
	}

	// VDD_TCXO
	val = HAL_GPIO_ReadPin(VDD_TCXO_GPIO_Port, VDD_TCXO_Pin);
	SEGGER_RTT_printf(0, "VDD_TCXO [0]: %d\n\r", val);
	if (GPIO_PIN_RESET != val) {
		error++;
	}

	HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED);
	SEGGER_RTT_printf(0, "ADC cal: %d\n\r", HAL_ADC_GetValue(&hadc));
	// VBAT_MEAS_ON
	if (HAL_GPIO_ReadPin(VBAT_MEAS_ON_GPIO_Port, VBAT_MEAS_ON_Pin)) {
		SEGGER_RTT_printf(0, "VBAT_MEAS_ON already high\n\r");
	} else {
		HAL_GPIO_WritePin(VBAT_MEAS_ON_GPIO_Port, VBAT_MEAS_ON_Pin,
				GPIO_PIN_SET);
	}
	HAL_Delay(15);

	// VABT ADC
	HAL_ADC_Start(&hadc);
	if (HAL_ADC_PollForConversion(&hadc, 100) == HAL_OK) {
		SEGGER_RTT_printf(0, "ADC: %d\n\r", HAL_ADC_GetValue(&hadc));
	} else {
		SEGGER_RTT_printf(0, "ADC: fail\n\r");
	}
	HAL_ADC_Stop(&hadc);

	// I2C Interface
	if (HAL_OK != HAL_I2C_Master_Receive(&hi2c1, 0xd4, NULL, 1, 10)) {
		SEGGER_RTT_printf(0, "I2C error\n\r");
		error++;
	}

	HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_1);

	SEGGER_RTT_printf(0, "%d errors\n\r", error);
	for (uint8_t i=error; i > 0; i--) {
		HAL_GPIO_WritePin(STATUS_GPIO_Port, STATUS_Pin, GPIO_PIN_RESET);
		HAL_Delay(250);
		HAL_GPIO_WritePin(STATUS_GPIO_Port, STATUS_Pin, GPIO_PIN_SET);
		HAL_Delay(250);
	}

}

uint8_t POST_status() {
	return (error == 0);
}
