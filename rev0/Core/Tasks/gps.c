/*
 * gps.c - GPS and UART code
 *
 *  Created on: 27.05.2020
 *      Author: bastian
 */
#include "main.h"

char sentence[80];
uint8_t sptr;

void process_char(char c){
	if (sptr == 0 && c == '$'){
		sentence[sptr] = c;
		sptr++;
	} else {
		sentence[sptr] = c;
		sptr++;
		if (c == '\n'){
			// Satz zuende -> parser kopieren
			SEGGER_RTT_printf(0, "NMEA: %s", sentence);
			sptr = 0;
	    }
	}
}
