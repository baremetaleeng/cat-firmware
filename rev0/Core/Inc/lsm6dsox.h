/*
 * lsm6dsox.h
 *
 *  Created on: May 27, 2020
 *      Author: bastian
 */

#ifndef INC_LSM6DSOX_H_
#define INC_LSM6DSOX_H_

#define LSM6DSOX_DEV_ADDR 0xD4

#define LSM6DSOX_WHO_AM_I 0x0F // R - 0x6C
#define LSM6DSOX_STATUS_REG 0x1E // R
#define LSM6DSOX_CTRL1_XL 0x10 // WR - Accelerometer control register 1

/*
 * Ab Register reg auslesen und in pData schreiben.
 * Gibt HAL_OK zurück.
 */
HAL_StatusTypeDef LSM_read(uint8_t reg, uint8_t *pData, uint8_t len);

/*
 * Schreibe Wert in Register
 * Gibt HAL_OK zurück
 */
HAL_StatusTypeDef LSM_write(uint8_t reg, uint8_t val);


#endif /* INC_LSM6DSOX_H_ */
