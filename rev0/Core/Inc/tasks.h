/*
 * tasks.h
 *
 *  Created on: May 25, 2020
 *      Author: bastian
 */

#ifndef INC_TASKS_H_
#define INC_TASKS_H_

#include <stdint.h>

void POST_run();
uint8_t POST_status();

// UART Zeichen verarbeiten
void process_char(char c);

#endif /* INC_TASKS_H_ */
