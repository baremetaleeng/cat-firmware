/*
 * lsm6dsox.c - IMU code
 *
 *  Created on: May 27, 2020
 *      Author: bastian
 */

#include "main.h"
extern I2C_HandleTypeDef hi2c1;

HAL_StatusTypeDef LSM_read(uint8_t reg, uint8_t *pData, uint8_t len) {
	if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c1, LSM6DSOX_DEV_ADDR, &reg, 1, 1)) {
			  return HAL_I2C_Master_Receive(&hi2c1, LSM6DSOX_DEV_ADDR, pData, len, 1);
	}
	return HAL_ERROR;
}


HAL_StatusTypeDef LSM_write(uint8_t reg, uint8_t val){
	uint8_t payload[2];
	payload[0] = reg;
	payload[1] = val;
	return HAL_OK == HAL_I2C_Master_Transmit(&hi2c1, LSM6DSOX_DEV_ADDR, payload, 2, 1);
}
