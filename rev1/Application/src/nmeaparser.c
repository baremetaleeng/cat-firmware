/*
 * nmeaparser.c
 *
 *  Created on: 17.06.2020
 *      Author: bastian
 */
#include <stdint.h>
#include <string.h>
#include "gps.h"
#include "util_console.h"

#include "SEGGER_RTT.h"

char sentence[81];
char *pSentence = sentence;

void parseSentence() {
	if (minmea_sentence_id(sentence, false) == MINMEA_SENTENCE_GGA) {
		if (minmea_parse_gga(&gga_frame, sentence)) {
			//PRINTF("GGA: fix quality: %d, sats: %d\n", gga_frame.fix_quality, gga_frame.satellites_tracked);
		} else {
			//PRINTF("GGA sentence is not parsed\n");
		}
	}
}

/*
 * Called from ISR processes one char
 */
void recvSentence(char c) {
	//  $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
	if (c == '$'){ // start sentence
		memset(sentence, 0, sizeof(sentence));
		pSentence = sentence;
	}

	*pSentence = c;

	if (c == '\n'){
		//SEGGER_RTT_printf(0, "GPS: %s", sentence);
		parseSentence();
	}

	pSentence++; // next buffer char
}

