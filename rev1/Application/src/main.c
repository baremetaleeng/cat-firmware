/**
  ******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power_manager.h"
#include "lora.h"
#include "bsp.h"
#include "timeServer.h"
#include "vcom.h"
#include "version.h"
#include "gps.h"
#include "SEGGER_RTT.h"
#include "string.h"
#include "w25qxx.h"
#include "lsm6dsrx_reg.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define LORAWAN_MAX_BAT   254
uint8_t UART_buf;
UART_HandleTypeDef huart1;
I2C_HandleTypeDef hi2c1;
volatile uint8_t trackingActive = 0;
volatile uint8_t trackingType = TRACKING_PERMANENT;
volatile uint8_t minHDOP = 60;
volatile uint8_t start_bootloader = 0;
/*!
 * Defines the application data transmission duty cycle. 60/10s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                            60000
#define APP_TX_SHORT_DUTYCYCLE                      10000
uint32_t txDutyCycle = APP_TX_DUTYCYCLE;

		/*!
 * Defines the heartbeat data transmission duty cycle. 60/300s, value in [ms].
 */
#define HB_TX_DUTYCYCLE								60000
#define HB_TX_LONG_DUTYCYCLE						300000
/*!
 * LoRaWAN Adaptive Data Rate
 * @note Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_STATE LORAWAN_ADR_ON
/*!
 * LoRaWAN Default data Rate Data Rate
 * @note Please note that LORAWAN_DEFAULT_DATA_RATE is used only when ADR is disabled
 */
#define LORAWAN_DEFAULT_DATA_RATE DR_0
/*!
 * LoRaWAN application port
 * @note do not use 224. It is reserved for certification
 */
#define LORAWAN_APP_PORT                            2
/*!
 * LoRaWAN default endNode class port
 */
#define LORAWAN_DEFAULT_CLASS                       CLASS_A
/*!
 * LoRaWAN default confirm state
 */
#define LORAWAN_DEFAULT_CONFIRM_MSG_STATE           LORAWAN_UNCONFIRMED_MSG
/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFF_SIZE                           64
/*!
 * User application data
 */
static uint8_t AppDataBuff[LORAWAN_APP_DATA_BUFF_SIZE];

#ifdef DEBUG
#define SOFTWARE_VERSION							 255
#else
#define SOFTWARE_VERSION                             (uint8_t)(__APP_VERSION >> 24)
#endif


const char set_led_0_dc[] = {
		0xBA, 0xCE, 0x10, 0x00, 0x06, 0x03, 0xA0, 0x86, 0x01, 0x00, 0xA0, 0x86, 0x01,
		0x00, 0x00, 0x00, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x50, 0x0D, 0x0A, 0x08
};
/*!
 * User application data structure
 */
//static lora_AppData_t AppData={ AppDataBuff,  0 ,0 };
lora_AppData_t AppData = { AppDataBuff,  0, 0 };

/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* call back when LoRa endNode has received a frame*/
static void LORA_RxData(lora_AppData_t *AppData);

/* call back when LoRa endNode has just joined*/
static void LORA_HasJoined(void);

/* call back when LoRa endNode has just switch the class*/
static void LORA_ConfirmClass(DeviceClass_t Class);

/* call back when server needs endNode to send a frame*/
static void LORA_TxNeeded(void);

/* callback to get the battery level in % of full charge (254 full charge, 0 no charge)*/
static uint8_t LORA_GetBatteryLevel(void);

/* LoRa endNode send request*/
static void Send(void *context);

/* start the tx process*/
static void LoraStartTx(TxEventType_t EventType);

/* tx timer callback function*/
static void OnTxTimerEvent(void *context);

/* tx timer callback function*/
static void LoraMacProcessNotify(void);

/* hb timer callback function*/
static void HeartbeatProcessNotify(void *context);

/* Private variables ---------------------------------------------------------*/
/* load Main call backs structure*/
static LoRaMainCallback_t LoRaMainCallbacks = { LORA_GetBatteryLevel,
                                                HW_GetTemperatureLevel,
                                                HW_GetUniqueId,
                                                HW_GetRandomSeed,
                                                LORA_RxData,
                                                LORA_HasJoined,
                                                LORA_ConfirmClass,
                                                LORA_TxNeeded,
                                                LoraMacProcessNotify
                                              };
LoraFlagStatus LoraMacProcessRequest = LORA_RESET;
LoraFlagStatus AppProcessRequest = LORA_RESET;
LoraFlagStatus HeartbeatProcessRequest = LORA_RESET;
/*!
 * Specifies the state of the application LED
 */

static TimerEvent_t TxTimer;
static TimerEvent_t HeartBeatTimer;

/* !
 *Initialises the Lora Parameters
 */
static  LoRaParam_t LoRaParamInit = {LORAWAN_ADR_STATE,
                                     LORAWAN_DEFAULT_DATA_RATE,
                                     LORAWAN_PUBLIC_NETWORK
                                    };

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /* STM32 HAL library initialization*/
  HAL_Init();

  /* Configure the system clock*/
  SystemClock_Config();
#ifdef DEBUG
  SEGGER_RTT_Init();
#endif
  /* Configure the debug mode*/
  DBG_Init();

  /* Configure the hardware*/
  HW_Init();

  /* USER CODE BEGIN 1 */
  while ( !HAL_GPIO_ReadPin(POWER_PG_GPIO_Port, POWER_PG_Pin)) {
	  PRINTF("WAIT FOR POWER GOOD\r\n");
	  HAL_Delay(1000);
  }

  PRINTF("POWER GOOD\r\n");
  //HAL_GPIO_WritePin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin, GPIO_PIN_RESET);

  /* USER CODE END 1 */
  LED_Off(LED_RED1);
  /*Disbale Stand-by mode*/
  LPM_SetOffMode(LPM_APPLI_Id, LPM_Disable);

  PRINTF("APP_VERSION= %02X.%02X.%02X.%02X\r\n", (uint8_t)(__APP_VERSION >> 24), (uint8_t)(__APP_VERSION >> 16), (uint8_t)(__APP_VERSION >> 8), (uint8_t)__APP_VERSION);
  PRINTF("MAC_VERSION= %02X.%02X.%02X.%02X\r\n", (uint8_t)(__LORA_MAC_VERSION >> 24), (uint8_t)(__LORA_MAC_VERSION >> 16), (uint8_t)(__LORA_MAC_VERSION >> 8), (uint8_t)__LORA_MAC_VERSION);

  /* Configure the Lora Stack*/
  LORA_Init(&LoRaMainCallbacks, &LoRaParamInit);

  LORA_Join();

  LoraStartTx(TX_ON_TIMER) ;

  buzz(100);

  // Initialize HeartBeat Timer and kick off first heartbeat
  TimerInit(&HeartBeatTimer, HeartbeatProcessNotify);
  TimerSetValue(&HeartBeatTimer,  HB_TX_DUTYCYCLE);
  TimerStart(&HeartBeatTimer);
  HeartbeatProcessRequest = LORA_SET;

  while (1)
  {
    if (AppProcessRequest == LORA_SET)
    {
      /*reset notification flag*/
      AppProcessRequest = LORA_RESET;
      /*Send*/
      Send(NULL);
    }
    if (LoraMacProcessRequest == LORA_SET)
    {
      /*reset notification flag*/
      LoraMacProcessRequest = LORA_RESET;
      LoRaMacProcess();
    }
    if (HeartbeatProcessRequest == LORA_SET)
    {

    	if (LORA_JoinStatus() == LORA_SET) {
           HeartbeatProcessRequest = LORA_RESET;
    	   AppData.Port = 1;
    	   AppData.Buff[0] = HW_GetBatteryLevel();
    	   AppData.Buff[1] = 0;
    	   AppData.Buff[1] += trackingActive ? 1 : 0;
    	   AppData.Buff[1] += trackingType ? 2 : 0;
    	   AppData.Buff[1] += HAL_GPIO_ReadPin(STATUS_GPIO_Port, STATUS_Pin) ? 4 : 0;
    	   AppData.Buff[1] += HAL_GPIO_ReadPin(CHRG_GPIO_Port, CHRG_Pin) ? 8 : 0;
    	   AppData.Buff[1] += start_bootloader ? 16 : 0;
    	   AppData.Buff[2] = SOFTWARE_VERSION;
    	   AppData.Buff[3] = gga_frame.satellites_tracked;
    	   AppData.BuffSize = 4;
    	   LORA_send(&AppData, LORAWAN_DEFAULT_CONFIRM_MSG_STATE);
    	   if(start_bootloader){
    		   buzz(100);
    		   HAL_Delay(100);
    		   buzz(100);
    		   to_bootloader();
    	   }
       }
    }

    /*If a flag is set at this point, mcu must not enter low power and must loop*/
    DISABLE_IRQ();

    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending
     * and cortex will not enter low power anyway  */
    if ((LoraMacProcessRequest != LORA_SET) && (AppProcessRequest != LORA_SET) && (!HAL_GPIO_ReadPin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin)))
    {
#ifndef LOW_POWER_DISABLE
      LPM_EnterLowPower();
#endif
    }

    ENABLE_IRQ();

    /* USER CODE BEGIN 2 */
    /* USER CODE END 2 */
  }
}


void LoraMacProcessNotify(void)
{
  LoraMacProcessRequest = LORA_SET;
}

static void HeartbeatProcessNotify(void *context)
{
  UNUSED(context);
	HeartbeatProcessRequest = LORA_SET;
  // if tracking is active we want to send in long duty cycle
/*  if(trackingActive){
	  TimerSetValue(&HeartBeatTimer, HB_TX_LONG_DUTYCYCLE);
  } else {*/
	  TimerSetValue(&HeartBeatTimer, HB_TX_DUTYCYCLE);
  /*}*/
  TimerStart(&HeartBeatTimer);
}

static void LORA_HasJoined(void)
{
#if( OVER_THE_AIR_ACTIVATION != 0 )
  PRINTF("JOINED\n\r");
#endif
  LORA_RequestClass(LORAWAN_DEFAULT_CLASS);
}

static void Send(void *context)
{
  /* USER CODE BEGIN 3 */
 UNUSED(context);
	sensor_t sensor_data;

  if (LORA_JoinStatus() != LORA_SET)
  {
    /*Not joined, try again later*/
    LORA_Join();
    TimerSetValue(&TxTimer, APP_TX_SHORT_DUTYCYCLE);
    TimerStart(&TxTimer);
    return;
  }

  TVL1(PRINTF("SEND REQUEST\n\r");)

  if(!trackingActive) {
	  // Make sure GPS is OFF
	  HAL_GPIO_WritePin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin, GPIO_PIN_RESET);
	TimerStart(&TxTimer);
	return;
  }

  if(!HAL_GPIO_ReadPin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin)) {
	  // turn GPS on
	  HAL_GPIO_WritePin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin, GPIO_PIN_SET);
	  HAL_Delay(1000);
	  HAL_UART_Transmit(&huart1, (uint8_t*)"$PCAS04,1*18\r\n", 14, 100);
	  // This is standby code for 10 seconds and it breaks the chip maybe?
	  //HAL_UART_Transmit(&huart1, (uint8_t*)"$PCAS12,10*2F\r\n", 15, 100);
	  //HAL_UART_Transmit(&huart1, set_led_0_dc, sizeof(set_led_0_dc), 100);
  }
#ifdef DEBUG
  TimerInit(&TxLedTimer, OnTimerLedEvent);

  TimerSetValue(&TxLedTimer, 200);

  LED_On(LED1) ;

  TimerStart(&TxLedTimer);
#else
  // Turn off led in production code
  LED_Off(LED_RED1);
#endif

  BSP_sensor_Read(&sensor_data);

  AppData.Port = LORAWAN_APP_PORT;

  if(sensor_data.valid) {
	  // turn off GPS
	  if(trackingType == TRACKING_PERIODIC)
	    HAL_GPIO_WritePin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin, GPIO_PIN_RESET);
	  // set longer duty cycle
	  TimerSetValue(&TxTimer, txDutyCycle);

	  // Copy all 12 bytes from sensor data to send buffer
	  memcpy(AppData.Buff, &sensor_data, sizeof(sensor_data));
	  sensor_data.valid = 0;
	  gga_frame.fix_quality = 0;
	  AppData.BuffSize = 12;


	  LORA_send(&AppData, LORAWAN_DEFAULT_CONFIRM_MSG_STATE);
  } else {
	  PRINTF("NO VALID DATA.\n\r");
	  TimerSetValue(&TxTimer, APP_TX_SHORT_DUTYCYCLE);
  }
  // Restart Timer
  TimerStart(&TxTimer);
  /* USER CODE END 3 */
}


static void LORA_RxData(lora_AppData_t *AppData)
{
  /* USER CODE BEGIN 4 */
  PRINTF("PACKET RECEIVED ON PORT %d\n\r", AppData->Port);
  PRINTF("PACKET %X:%X:%X\n\r", AppData->Buff[0],AppData->Buff[1],AppData->Buff[2]);

  if(AppData->Port == 1)
  {
	  // Test for bit 1 in first byte for activation
	  if(AppData->Buff[0] & 0x01){
         PRINTF("TRACKING ACTIVATED \n\r");
         trackingActive = 1;
	  } else {
		 PRINTF("TRACKING DEACTIVATED \n\r");
         trackingActive = 0;
	  }
	  // Test for bit 2 in first byte for permanent GPS tracking
	  if(AppData->Buff[0] & 0x02){
         PRINTF("TRACKING PERMANENT \n\r");
         trackingType = TRACKING_PERMANENT;
	  } else {
		 PRINTF("TRACKING PERIODIC \n\r");
         trackingType = TRACKING_PERIODIC;
	  }
	  // Test for bit 3 in first byte for signal status
	  if(AppData->Buff[0] & 0x04){
         PRINTF("SIGNAL ON \n\r");
         HAL_GPIO_WritePin(STATUS_GPIO_Port, STATUS_Pin, GPIO_PIN_RESET);
         buzz(200);
         HAL_Delay(50);
         buzz(200);
	  } else {
		 PRINTF("SIGNAL OFF \n\r");
         HAL_GPIO_WritePin(STATUS_GPIO_Port, STATUS_Pin, GPIO_PIN_SET);
	  }

	  if(AppData->Buff[0] &0x80){
         PRINTF("TO BOOTLOADER ON NEXT STATUS\r\n");
         buzz(500);
         start_bootloader = 1;
	  }

	  // Set HDOP level
	  minHDOP = AppData->Buff[1];
	  PRINTF("SET MINHDOP: %d\n\r", minHDOP);
	  // Set TX Duty Cycle in 10000ms
	  txDutyCycle = AppData->Buff[2] * 10000;
	  PRINTF("SET TX DUTYCYCLE: %d\n\r", txDutyCycle);

  }
  /* USER CODE END 4 */
}

static void OnTxTimerEvent(void *context)
{
	UNUSED(context);
  /*Wait for next tx slot*/
  // do that after Send
  //TimerStart(&TxTimer);
  AppProcessRequest = LORA_SET;
  if (!HAL_GPIO_ReadPin(STATUS_GPIO_Port, STATUS_Pin))
	  buzz(200);
}

static void LoraStartTx(TxEventType_t EventType)
{
  if (EventType == TX_ON_TIMER)
  {
    /* send everytime timer elapses */
    TimerInit(&TxTimer, OnTxTimerEvent);
    TimerSetValue(&TxTimer,  APP_TX_DUTYCYCLE);
    OnTxTimerEvent(NULL);
  }
}

static void LORA_ConfirmClass(DeviceClass_t Class)
{
#ifndef DEBUG
	UNUSED(Class);
#endif
	PRINTF("switch to class %c done\n\r", "ABC"[Class]);

  /*Optionnal*/
  /*informs the server that switch has occurred ASAP*/
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send(&AppData, LORAWAN_UNCONFIRMED_MSG);
}

static void LORA_TxNeeded(void)
{
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send(&AppData, LORAWAN_UNCONFIRMED_MSG);
}

/**
  * @brief This function return the battery level
  * @param none
  * @retval the battery level  1 (very low) to 254 (fully charged)
  */
uint8_t LORA_GetBatteryLevel(void)
{
  uint16_t batteryLevelmV;
  uint8_t batteryLevel = 0;

  batteryLevelmV = HW_GetBatteryLevel();


  /* Convert batterey level from mV to linea scale: 1 (very low) to 254 (fully charged) */
  if (batteryLevelmV > VDD_BAT)
  {
    batteryLevel = LORAWAN_MAX_BAT;
  }
  else if (batteryLevelmV < VDD_MIN)
  {
    batteryLevel = 0;
  }
  else
  {
    batteryLevel = (((uint32_t)(batteryLevelmV - VDD_MIN) * LORAWAN_MAX_BAT) / (VDD_BAT - VDD_MIN));
  }

  return batteryLevel;
}

extern void recvSentence(char c);
/**
 * @brief This function handles USART1 global interrupt / USART1 wake-up interrupt through EXTI line 25.
 */
void USART1_IRQHandler(void) {
	/* USER CODE BEGIN USART1_IRQn 0 */

	/* USER CODE END USART1_IRQn 0 */
	HAL_UART_IRQHandler(&huart1);
	/* USER CODE BEGIN USART1_IRQn 1 */
	recvSentence(UART_buf);
	HAL_UART_Receive_IT(&huart1, &UART_buf, 1);
	/* USER CODE END USART1_IRQn 1 */
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
