/*
 * Set system into bootloader mode
 *
 *  Created on: Nov 2, 2020
 *      Author: bastian
 */

//#include "stm32l072xx.h"
#include "stm32l0xx_hal.h"
#include "mlm32l0xx_hw_conf.h"

#define MAGIC_ADDR   0x20002000
#define MAGIC_NUMBER1 0xB00710AD
#define MAGIC_NUMBER2 0x10ADB007

#define BOOTLOADER_STACK         *(__IO uint32_t *)0x1FF00000
#define BOOTLOADER_JUMP_ADDRESS1 *(__IO uint32_t *)0x1FF00004
#define BOOTLOADER_JUMP_ADDRESS2                   0x1FF00369


void to_bootloader() {
	*((unsigned long*) MAGIC_ADDR) = MAGIC_NUMBER1;
	HAL_NVIC_SystemReset();
}

typedef void (*pFunction)(void);
pFunction BootloaderJump;

void jump_to_bootloader(void) {
	HAL_DeInit();

	if (*((unsigned long*) MAGIC_ADDR) == MAGIC_NUMBER1) {
		/* change the magic number */
		*((unsigned long*) MAGIC_ADDR) = MAGIC_NUMBER2;
		  __set_MSP(BOOTLOADER_STACK);
		  BootloaderJump = (pFunction)(BOOTLOADER_JUMP_ADDRESS1);
		  BootloaderJump();
	}

	if (*((unsigned long*) MAGIC_ADDR) == MAGIC_NUMBER2) {
		/* erase the magic number */
		*((unsigned long*) MAGIC_ADDR) = 0xFFFFFFFF;
		  __set_MSP(BOOTLOADER_STACK);
		  BootloaderJump = (pFunction)(BOOTLOADER_JUMP_ADDRESS2);
		  BootloaderJump();
	}
}

