/**
  ******************************************************************************
  * @file    bsp.c
  * @author  MCD Application Team
  * @brief   manages the sensors on the application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "timeServer.h"
#include "bsp.h"
#include "gps.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define STSOP_LATTITUDE ((float) 49.6427027 )
#define STSOP_LONGITUDE ((float) 8.5201724  )
#define MAX_GPS_POS ((int32_t) 8388607  ) // 2^23 - 1
#define STSOP_ALTITUDE 100 /* 100m über N.N */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Exported functions ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
extern UART_HandleTypeDef huart1;
extern uint8_t UART_buf;
extern uint8_t minHDOP;

extern I2C_HandleTypeDef hi2c1;
extern void ACC_IRQHandler_INT1();
extern void ACC_IRQHandler_INT2();


void BSP_sensor_Read(sensor_t *sensor_data)
{
	/* USER CODE BEGIN 5 */
	// HDOP < 6 is good value (hdop has scale 10)
  sensor_data->valid = gga_frame.hdop.value <= minHDOP ? gga_frame.hdop.value : 0;

  sensor_data->latitude  = minmea_tocoord(&gga_frame.latitude) * 10000000;
  sensor_data->longitude = minmea_tocoord(&gga_frame.longitude) * 10000000;
  sensor_data->altitudeGps = gga_frame.altitude.value ;
  sensor_data->satelitesGps = gga_frame.satellites_tracked;
  sensor_data->hdop = gga_frame.hdop.value;

  // mark gga_frame as invalid
  memset(&gga_frame, 0, sizeof(gga_frame));

  /* USER CODE END 5 */
}

void  BSP_sensor_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	  huart1.Instance = USART1;
	  huart1.Init.BaudRate = 9600;
	  huart1.Init.WordLength = UART_WORDLENGTH_8B;
	  huart1.Init.StopBits = UART_STOPBITS_1;
	  huart1.Init.Parity = UART_PARITY_NONE;
	  huart1.Init.Mode = UART_MODE_TX_RX;
	  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&huart1) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  // Get first Char
	  HAL_UART_Receive_IT(&huart1, &UART_buf, 1);

	// GPS Modul nicht einschalten
	  __HAL_RCC_GPIOB_CLK_ENABLE();
	  HAL_GPIO_WritePin(POWER_CTRL_GPIO_Port, POWER_CTRL_Pin, GPIO_PIN_RESET);

	  /*Configure GPIO pin : POWER_PG_Pin */
	  GPIO_InitStruct.Pin = POWER_PG_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  HAL_GPIO_Init(POWER_PG_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : POWER_CTRL_Pin */
	  GPIO_InitStruct.Pin = POWER_CTRL_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(POWER_CTRL_GPIO_Port, &GPIO_InitStruct);

	  /* COnfigure GPIO pin : CHRG_Pin */
	  GPIO_InitStruct.Pin = CHRG_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(CHRG_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pins : IMU_INT2_Pin IMU_INT1_Pin */
	  GPIO_InitStruct.Pin = IMU_INT2_Pin|IMU_INT1_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	  // Initialize I2C1 for ACC and GYRO
	  hi2c1.Instance = I2C1;
	  hi2c1.Init.Timing = 0x00300F38;
	  hi2c1.Init.OwnAddress1 = 0;
	  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	  hi2c1.Init.OwnAddress2 = 0;
	  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Configure Analogue filter
	  */
	  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Configure Digital filter
	  */
	  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  // TODO: Interrupt wieder einschalten
	  //HW_GPIO_SetIrq(GPIOB, IMU_INT1_Pin, 0, ACC_IRQHandler_INT1);
	  //HW_GPIO_SetIrq(GPIOB, IMU_INT2_Pin, 0, ACC_IRQHandler_INT2);

}

uint8_t Sensor_IO_Write( void *handle, uint8_t WriteAddr, uint8_t *pBuffer, uint16_t nBytesToWrite ) {
#warning Determine buffer size
	uint8_t transBuffer[255];
	transBuffer[0] = WriteAddr;
	memcpy(transBuffer+1, pBuffer, nBytesToWrite);
	return HAL_I2C_Master_Transmit(&hi2c1, 0xd4, pBuffer, nBytesToWrite + 1, 1000);
}

uint8_t Sensor_IO_Read( void *handle, uint8_t ReadAddr, uint8_t *pBuffer, uint16_t nBytesToRead ) {

	if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c1, 0xd4, &ReadAddr, 1, 1)) {
	return HAL_I2C_Master_Receive(&hi2c1, 0xd4, pBuffer, nBytesToRead, 1000);
	}
	return HAL_ERROR;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
