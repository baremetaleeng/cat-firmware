/*
 * Accelerator code
 *
 *  Created on: 09.09.2020
 *      Author: bastian
 */
#include <string.h>

#include "hw_acc.h"
#include "stm32l0xx_hal.h"
#include "lsm6dsrx_reg.h"

#include "w25qxx.h"

#include "util_console.h"
#include "SEGGER_RTT.h"

extern I2C_HandleTypeDef hi2c1;
stmdev_ctx_t hacc1;
lsm6dsrx_pin_int1_route_t int1_route;
lsm6dsrx_pin_int2_route_t int2_route;

uint8_t *page;
uint8_t page_count = 0;
uint8_t page_sum = 0;
uint8_t buf1[256] = {};
uint8_t buf2[256] = {};


static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
		uint16_t len) {
	HAL_I2C_Mem_Read(&hi2c1, LSM6DSRX_I2C_ADD_L, reg,
	I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
	return 0;
}

static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
		uint16_t len) {
	HAL_I2C_Mem_Write(&hi2c1, LSM6DSRX_I2C_ADD_L, reg,
	I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
	return 0;
}

void HW_ACC_Init() {
	uint8_t whoAmI, rst;
	lsm6dsrx_pin_int1_route_t int1;

	hacc1.write_reg = platform_write;
	hacc1.read_reg = platform_read;
	hacc1.handle = &hi2c1;


	lsm6dsrx_device_id_get(&hacc1, &whoAmI);
	if (whoAmI != LSM6DSRX_ID)
		PRINTF("LSMDSX not found\r\n");

	/* Restore default configuration */
	lsm6dsrx_reset_set(&hacc1, PROPERTY_ENABLE);
	do {
		lsm6dsrx_reset_get(&hacc1, &rst);
	} while (rst);

	  /* Disable I3C interface. */
	  lsm6dsrx_i3c_disable_set(&hacc1, LSM6DSRX_I3C_DISABLE);

	  /* Enable Block Data Update. */
	  lsm6dsrx_block_data_update_set(&hacc1, PROPERTY_ENABLE);

	  /* Set Output Data Rate. */
	  lsm6dsrx_xl_data_rate_set(&hacc1, LSM6DSRX_XL_ODR_26Hz);
	  lsm6dsrx_gy_data_rate_set(&hacc1, LSM6DSRX_GY_ODR_26Hz);

	  /* Set full scale. */
	  lsm6dsrx_xl_full_scale_set(&hacc1, LSM6DSRX_4g);
	  lsm6dsrx_gy_full_scale_set(&hacc1, LSM6DSRX_2000dps);


	  /* Enable timestamp. */
	  lsm6dsrx_timestamp_set(&hacc1, PROPERTY_ENABLE);

	  /* Configure filtering chain(No aux interface)
	   * Accelerometer - LPF1 + LPF2 path
	   */
	  //lsm6dsrx_xl_hp_path_on_out_set(&hacc1, LSM6DSRX_LP_ODR_DIV_100);
	  //lsm6dsrx_xl_filter_lp2_set(&hacc1, PROPERTY_ENABLE);

	  lsm6dsrx_data_ready_mode_set(&hacc1, LSM6DSRX_DRDY_PULSED);

	  lsm6dsrx_pin_int2_route_get(&hacc1, &int2_route);
	  int2_route.int2_ctrl.int2_drdy_xl = PROPERTY_ENABLE;
	  int2_route.int2_ctrl.int2_drdy_g = PROPERTY_ENABLE;
	  lsm6dsrx_pin_int2_route_set(&hacc1, &int2_route);


}

void Save_ACC_Data(acc_data_t *data) {
	lsm6dsrx_reg_t reg;


    /* Read output only if new value is available. */
    lsm6dsrx_status_reg_get(&hacc1, &reg.status_reg);

    if (reg.status_reg.xlda || reg.status_reg.gda)
      lsm6dsrx_timestamp_raw_get(&hacc1, &data->timestamp);

    if (reg.status_reg.xlda)
    {
      /* Read acceleration field data */
      lsm6dsrx_acceleration_raw_get(&hacc1, data->acc);
      data->flags |= ACC;
    }

    if (reg.status_reg.gda)
    {
      /* Read angular rate field data */
      lsm6dsrx_angular_rate_raw_get(&hacc1, data->gyr);
      data->flags |= GYRO;
    }
}



uint8_t Read_Activity() {
	lsm6dsrx_all_sources_t all_source;

	/* Check if Activity/Inactivity events */
	lsm6dsrx_all_sources_get(&hacc1, &all_source);
	if (all_source.wake_up_src.sleep_state) {
		PRINTF("Inactivity Detected\r\n");
	}

	if (all_source.wake_up_src.wu_ia) {
		PRINTF("Activity Detected\r\n");
	}

}

void Read_Acceleration(int16_t acceleration[3]){
	uint8_t reg;
	int16_t data_raw_acceleration[3] = {};
	lsm6dsrx_acceleration_raw_get(&hacc1, data_raw_acceleration);
	for(uint8_t i= 0;i<3;i++){
		acceleration[i] = (data_raw_acceleration[i] * 122 / 1000);
	}
}

void Read_Gyro(int16_t rotation[3]){
	uint8_t reg;
	int16_t data_raw_rotation[3] = {};
	lsm6dsrx_angular_rate_raw_get(&hacc1, data_raw_rotation);
	for(uint8_t i= 0;i<3;i++){
		rotation[i] = data_raw_rotation[i] * 140;
	}
}

uint16_t Read_Temperature() {
	lsm6dsrx_reg_t reg;
	int16_t data_raw_temperature;
	uint32_t temperature_degC;
	/* Read output only if new value is available. */
	lsm6dsrx_status_reg_get(&hacc1, &reg.status_reg);

	if (reg.status_reg.tda) {
		/* Read temperature data */
		memset(&data_raw_temperature, 0x00, sizeof(int16_t));
		lsm6dsrx_temperature_raw_get(&hacc1, &data_raw_temperature);

		//return (((float_t)lsb / 256.0f) + 25.0f);

		data_raw_temperature *= 100;
		temperature_degC = data_raw_temperature / 256;
		temperature_degC += 2500;

		PRINTF("Temperature %x [degC]:%d\r\n", data_raw_temperature,
				temperature_degC);
	}
}

void ACC_IRQHandler_INT1() {

}

void ACC_IRQHandler_INT2() {
	lsm6dsrx_reg_t reg;
	union {
		struct values {
			uint32_t ts; // 4 byte
			int16_t acc[3]; // 6 byte
			int16_t gyr[3]; // 6 byte
			uint8_t magic;
		} values;
		uint8_t bytes[sizeof(struct values)];
	} data;

	data.values.magic = 0xa5;

	lsm6dsrx_status_reg_get(&hacc1, &reg.status_reg);
	if ( reg.status_reg.gda & reg.status_reg.xlda) {
		lsm6dsrx_timestamp_raw_get(&hacc1, &data.values.ts);
		Read_Gyro(data.values.gyr);
		Read_Acceleration(data.values.acc);

		// write data to page buffer
		if(page_count + sizeof(data) < 255){
			memcpy(buf1+page_count , &data.bytes, sizeof(data));
			page_count += sizeof(data);
		} else {
			W25qxx_WritePage(buf1, page_sum, 0, sizeof(buf1));
			page_sum++;
		}
	}
}

