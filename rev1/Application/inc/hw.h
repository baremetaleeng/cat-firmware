/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: contains all hardware driver

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
/**
  ******************************************************************************
  * @file    hw.h
  * @author  MCD Application Team
  * @brief   contains all hardware driver
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_H__
#define __HW_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "hw_conf.h"
#include "hw_gpio.h"
#include "hw_spi.h"
#include "hw_spi2.h"
#include "hw_rtc.h"
#include "hw_msp.h"
#include "hw_buzzer.h"
#include "hw_acc.h"
#include "util_console.h"
#include "debug.h"

#define VBAT_MEAS_U_Pin GPIO_PIN_2
#define VBAT_MEAS_U_GPIO_Port GPIOA
#define VBAT_MEAS_ON_Pin GPIO_PIN_3
#define VBAT_MEAS_ON_GPIO_Port GPIOA
#define STATUS_GPIO_Port GPIOA
#define STATUS_Pin GPIO_PIN_8
#define CHRG_GPIO_Port GPIOA
#define CHRG_Pin GPIO_PIN_5
#define EEPROM_WP_GPIO_Port GPIOA
#define EEPROM_WP_Pin GPIO_PIN_4
#define EEPROM_CS_GPIO_Port GPIOB
#define EEPROM_CS_Pin GPIO_PIN_12
#define IMU_INT1_Pin GPIO_PIN_7
#define IMU_INT1_GPIO_Port GPIOB
#define IMU_INT1_EXTI_IRQn EXTI4_15_IRQn
#define IMU_INT2_Pin GPIO_PIN_5
#define IMU_INT2_GPIO_Port GPIOB
#define IMU_INT2_EXTI_IRQn EXTI4_15_IRQn
#define POWER_PG_Pin GPIO_PIN_6
#define POWER_PG_GPIO_Port GPIOB
#define POWER_CTRL_Pin GPIO_PIN_2
#define POWER_CTRL_GPIO_Port GPIOB


#ifdef __cplusplus
}
#endif

#endif /* __HW_H__ */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

