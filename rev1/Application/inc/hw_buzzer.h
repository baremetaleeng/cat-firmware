/*
 * hw_buzzer.h
 *
 *  Created on: Aug 30, 2020
 *      Author: bastian
 */

#ifndef ___HW_BUZZER_H_
#define __HW_BUZZER_H_

void BSP_buzzer_Init();
void buzz();

#endif /* APPLICATION_INC_HW_BUZZER_H_ */
