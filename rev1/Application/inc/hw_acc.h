/*
 * hw_acc.h
 *
 *  Created on: 09.09.2020
 *      Author: bastian
 */

#ifndef __HW_ACC_H
#define __HW_ACC_H

#include <stdint.h>

void HW_ACC_Init();
enum flags {ACC=1, GYRO};

typedef struct {
	uint32_t timestamp;
	int flags;
	int16_t acc[3];
	int16_t gyr[3];
} acc_data_t;

void Save_ACC_Data();


#endif /* __HW_ACC_H */
