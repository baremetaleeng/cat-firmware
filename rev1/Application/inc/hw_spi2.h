/*
 * hw_spi2.h
 *
 *  Created on: 29.06.2020
 *      Author: bastian
 */

#ifndef __HW_SPI2_H_
#define __HW_SPI2_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
extern SPI_HandleTypeDef hspi2;
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/*!
 * @brief Initializes the SPI2 object and MCU peripheral
 *
 * @param [IN] none
 */
void HW_SPI2_Init(void);

/*!
 * @brief De-initializes the SPI object and MCU peripheral
 *
 * @param [IN] none
 */
void HW_SPI2_DeInit(void);

/*!
 * @brief Sends outData and receives inData
 *
 * @param [IN] outData Byte to be sent
 * @retval inData      Received byte.
 */
uint16_t HW_SPI2_InOut(uint16_t outData);


/*!
 * @brief Read Device ID
 *
 * @retval devID	Devide ID
 */
uint8_t HW_SPI_FLASH_ReadDeviceId();


#ifdef __cplusplus
}
#endif

#endif /* __HW_SPI2_H_ */
