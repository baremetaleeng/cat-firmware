/*
 * gps.h
 *
 *  Created on: 17.06.2020
 *      Author: bastian
 */

#ifndef PROJECTS_END_NODE_GPS_H_
#define PROJECTS_END_NODE_GPS_H_
#include "minmea.h"

struct minmea_sentence_gga gga_frame;

enum {
	TRACKING_PERMANENT,
	TRACKING_PERIODIC
};

#endif /* PROJECTS_END_NODE_GPS_H_ */
