# TODO

* [X] Debug Ausgabe auf SEGGER RTT umleiten
* [X] Debug Hardware Konfiguration entfernen
* [X] BSP anpassen
* [X] Payload
* [X] Sensorwerte "auslesen"
    - [X] GPS fake Daten -> 49.6427027 , 8.5201724
    - [X] ADC Batterie Spannung fake Daten
* [X] TTN Daten Interpretation
    - [X] int32_t max = 214.7483647
    - [X] GPS Koordinaten * 10000000
    - [X] Decoder Koordinaten / 10000000
* [X] TTN JOIN
    - [X] DEV EUI msb
    - [X] APP EUI msb
    - [X] AppKey msb
* [-] GPS Modul in Betrieb nehmen
    - [X] PG Pin auslesen
    - [X] CTRL Pin setzen
    - [X] 3,3V zum GPS Modul messen
    - [X] UART ISR -> $GPGGA
    - [X] GPS Daten in Sensorwerte kopieren
    - [X] GPS Daten in Dezimalwinkel konvertieren
    - [X] dynamischer Timer
* [X] ADC konfigurieren
    - [X] Konfiguration auf richtigen Kanal stellen
    - [X] Daten in Sensorwerte kopieren
    - [X] Pin für ADC konfigurieren (Msp)
    - [X] GPIO für Tansistor konfigurieren
* [X] Projekt für git erzeugen
* [X] Firmware ohne Debug und mit Deep Sleep bauen und hochladen
* [X] Payloads bestimmen
    - [X] Positions Payload
    - [X] Batterie Payload
    - [-] (Bewegungssensor Payload)
* [X] Ports festelgen
    - [X] Tracker Payload (2)
        - [X] Koordinate in 3D
        - [X] Genauigkeit
    - [X] Status Meldung (1)
        - [X] Batterie Zustand
        - [-] Bewegungssensor Klasse
* [ ] Heartbeat Paket
    - [/] Reset counter wenn Positionspaket gesendet wurde
    - [X] HeartbeatProcessRequest auf LORA_RESET!
    - [X] Dynamischer Timer für HB Pakete
* [X] ADC Skalierung
    - [X] In ADC Funktion
    - [X] Im Cloud Backend
* [X] Daten empfangen
    - [X] globale Flags
        - [X] Tracking durchführen
        - [X] GPS dauerhaft eingeschaltet lassen
        - [X] Minimale Genauigkeit für valides GPS Paket
        - [X] Sende Häufingkeit in Sekunden (max 255)
* [ ] GPS Modul beim Start der Applikation nicht einschalten
    - [ ] Konstellation bei jedem Modulstart übertragen
    - [ ] LED Einstellung an das Modul übertragen: BA CE 10 00 06 03 A0 86 01 00 A0 86 01 00 00 00 01 05 00 00 00 00 50 0D 0A 08 
* [ ] Besseres Status Paket
    - [ ] Schaltzustände (tracking GPS Modus)
    - [ ] Firmware Version (ohne Status Paket ist 1.0, diese wird 1.0.1)
* [ ] Umzug auf Bitbucket Issues

