# LSM6DSOX Infos

Der Bewegungs-/Rotationssensor auf dem CA Tracker ist der LSM6DSOX. Diese Datei
enthält Informationen über die Funktion und Konfiguration des Sensors.

# Konfiguration


# Interrupts
ALL_INT_SRC (0x1A) = 0x02
WAKE_UP_SRC (0x1B) = 0x0F
TAP_SRC     (0x1C) = 0x00

# ML Kern
- Bewegungsklassen
    - Schütteln
    - Rumliegen
